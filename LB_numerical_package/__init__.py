import numpy as np


# The algorithm for aquiring the newton coeff
def Newtoncoefficient(fi, xi):
    n = len(fi)
    coeff = initializecoefficient(fi, n)
    for j in range(1, n):
        for i in range(0, n - j):
            coeff[i][j] = (coeff[i + 1, j - 1] - coeff[i, j - 1]
                           ) / (xi[i + j] - xi[i])
    return coeff


def initializecoefficient(fi, n):
    coeff = np.zeros([n, n])
    for i in range(n):
        for j in range(n):
            if j == 0:
                coeff[i][j] = fi[i]
    return coeff


# The result of the interpolation in x. ATTENTION it returns the value of the
# interpolation in x and not a symbolic function
def poli_Newton(fi, xi, x):
    coeff = Newtoncoefficient(fi, xi)
    poli = coeff[0, 0]
    product = 1
    for i in range(1, len(xi)):
        product = product * (x - xi[i - 1])
        poli += coeff[0][i] * product
    return poli


def equidistant_array(start, finish, how_many):
    dist = [start]
    step = float(finish - start) / (how_many - 1)
    x = start
    for i in range(how_many - 1):
        x += step
        dist.append(x)
    return dist


# The trapezoid method
def trap_integral(f, interval):
    a, b = interval
    return abs(a - b) * (f(a) + f(b)) / 2.0


# The simpson 1/3 method
def simpson_1_3(f, interval):
    a, b = interval
    h = abs(a - b) / 2
    return (h / 3.0) * (f(a) + 4 * f((a + b) / 2) + f(b))


# The simpson 3/8 method
def simpson_3_8(f, interval):
    a, b = interval
    factor = (abs(a - b) * (3.0 / 8.0))
    x_1 = (2.0 * a + b) / 3.0
    x_2 = (a + 2.0 * b) / 3.0
    return factor * (f(a) + 3.0 * f(x_1) + 3.0 * f(x_2) + f(b)) / 3


# The composed method. BE CAREFUL N is not the total number of subintervals,
# but the number of intervals of which is applied the method chosen.
# for example, if we choose simpson 1/3, that function will be applied N times
# thus creating 2N subintervals. This was done for creating a more flexible
# function (and more readable).
def composed_method(f, interval, N, method=trap_integral):
    a, b = interval
    h = (b - a) / N
    start = 0
    integral = 0
    for i in range(N):
        integral += method(f, (start, start + h))
        start += h
    return integral


# The gauss legandre method. It doesn't need a variable N, as it's implicit
# in the vector point_weight that coitains the point and weight needed.
def gauss_legendre(f_gauss, point_weight):
    integral = 0
    for point, weight in point_weight:
        integral += f_gauss(point) * weight
    return integral


def read_and_get_gauss(file_name):
    file = open(file_name)
    lst = []
    for line in file:
        lst.append((float(string) for string in line.split()))
    return lst
# 'gauss_legendre/gauss_100_pontos.dat'


# This methods solve differential equation
# interval is a tuple with length 2 and it rapresents the ends of the
# indipendent variable (in our case t)
# N is the number of sub intervals used
# diff_fun is the differnetial function used and defined in the same method
# as the previous
# the initial_cond is a tuple that contains the initial conditions for all the
# variables, even the independent one.
def Rounge_Kutta_4(interval, N, diff_fun, initial_cond):
    h = abs(interval[1] - interval[0]) / float(N)
    t = np.zeros(N)
    x = np.zeros((len(initial_cond) - 1, N))
    initialize(t, x, initial_cond)
    for n in range(N - 1):
        # No need of running a cycle on the number of variables since we use
        # an ndarray format.
        k_1 = diff_fun(t[n], x[:, n])
        k_2 = diff_fun(t[n] + h / 2, x[:, n] + (h * k_1) / 2)
        k_3 = diff_fun(t[n] + h / 2, x[:, n] + (h * k_2) / 2)
        k_4 = diff_fun(t[n] + h, x[:, n] + h * k_3)
        x[:, n + 1] = x[:, n] + (h * (k_1 + 2 * k_2 + 2 * k_3 + k_4)) / 6
        t[n + 1] = t[n] + h
    return t, x


# It gives the initial conditions
def initialize(t, x, initial_cond):
    t[0] = initial_cond[0]
    for i in range(1, len(initial_cond)):
        x[i - 1][0] = initial_cond[i]


# By A we mean a numpy 2darray that we want to use to compute the determinant
# or solve with b an nx1 numpy array. solve_linear will solve Ax = b
# The variable pivot is used to add the partial pivot method in the functions


# THE MAIN FUNCTIONS

# It creates the hilbert matrix
def solve_linear(A, b, pivot=False):
    return backward_subs(*gauss_elimination(A, b))


def det(A, pivot=False):
    # I use the gauss elimination adding an empty array
    U, c = gauss_elimination(A, np.zeros((np.shape(A)[0], 1)), pivot=pivot)
    return np.prod(U.diagonal())


def gauss_elimination(A, b, pivot=False):
    A = np.hstack((A, b))
    for j in range(np.shape(A)[0] - 1):
        partial_pivot(A, j) if pivot else None
        # Here I remove to the the rows from j+1 the row j multiplied by the
        # elements on j column divided by the A[j,j]. Besically this is the
        # algorithm that needs to be repeted n - 1 times
        A[j + 1:][:] = A[j + 1:][:] - np.tile(A[j: j + 1], np.shape(
            A[j + 1:, 0][:, None])) * ((A[j + 1:, j][:, None] / A[j, j]))
    return A[:, 0:-1], A[:, -1:]


def partial_pivot(A, index_pivot):
    next_pivot = A[index_pivot:, index_pivot].argmax()
    if next_pivot != 0:
        # here I use advance slicing to swap the lines
        A[[index_pivot, index_pivot + next_pivot],
            :] = A[[index_pivot + next_pivot, index_pivot], :]


def backward_subs(A, c):
    x = np.empty(len(c))
    x[-1] = c[-1] / A[-1][-1]
    for k in range(len(c) - 2, -1, -1):
        x[k] = (c[k] - np.sum(A[k, k + 1:] * x[k + 1:])) / A[k][k]
    return x


def create_hilbert(n):
    H = np.empty([n, n])
    for i in range(1, n + 1):
        for j in range(1, n + 1):
            H[i - 1][j - 1] = 1. / (i + j - 1)
    return H
