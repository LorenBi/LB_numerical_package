from setuptools import setup


setup(name='LB_numerical_package',
      version='0.1',
      description='Numerical functions from course on comp physics',
      url='https://github.com/LorenzoBi/LB_numerical_package',
      author='LorenzoBi',
      author_email='___@__.it',
      license='MIT',
      packages=['LB_numerical_package'],
      zip_safe=False)
